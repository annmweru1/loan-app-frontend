import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { BankDetails } from '../models/loan.models';
import { LoanService } from '../services/loan.service';

@Component({
  selector: 'app-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.css']
})
export class BankFormComponent implements OnInit {
  title = "Bank"
  constructor(
    private formBuilder: FormBuilder,
    private loanService: LoanService,
    private router: Router,
  ) {}
  bankForm = this.formBuilder.group({
    accountName : "",
    accountNo: "",
    bankName: "",
    bankCode: "",
    branch: "",
    branchCode: "",
  })

  ngOnInit(){}

  onSubmit() :void {
    this.bankForm.reset()
    this.router.navigateByUrl("/apply/particulars")

    let bank = new BankDetails()
      bank.account_name = this.bankForm.value.accountName;
      bank.account_no = this.bankForm.value.accountNo
      bank.bank_Code = this.bankForm.value.bankCode
      bank.branch = this.bankForm.value.branch
      bank.branch_code = this.bankForm.value.branchCode
      bank.bank_Name = this.bankForm.value.bankName
  } 
}
