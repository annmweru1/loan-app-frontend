import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { Constant } from '../common/const.common';
import { Util } from '../common/util.common';
import { EmploymentDetails } from '../models/loan.models';
import { LoanService } from '../services/loan.service';

@Component({
  selector: 'app-employment-form',
  templateUrl: './employment-form.component.html',
  styleUrls: ['./employment-form.component.css']
})
export class EmploymentFormComponent  {

  util = new Util()
  title = "Employment"
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: LoanService
  ) {}
  employmentForm = this.formBuilder.group({
    applicantEmployer: "",
    physicalAddress: "",
    postalAddress: "",
    telephoneNumber: "",
    designation: "",
    staffNo: "",
    employmentTerms: "",
    typeOfBusiness: "",
    yearsOfOperation: "",
    businessIncome : "",
    rentalIncome: "",
    otherIncome: ""
  })

  onSubmit() :void {
   
    let employ = new EmploymentDetails()
     employ.applicant_employer = this.employmentForm .value.applicantEmployer;
     employ.physical_address = this.employmentForm .value.physicalAddress;
     employ.postal_address = this.employmentForm .value.postalAddress;
     employ.tel_phone = this.employmentForm .value.telephoneNumber;
     employ.designated = this.employmentForm .value.designation;
     employ.staff_number = this.employmentForm .value.staffNo;
     employ.employment_terms = this.employmentForm .value.employmentTerms;

      
     let user_id = this.util.getCookie(Constant.ID_COOKIE_KEY)

     employ.personal_particulars_id = Number(user_id);

     this.service.update(user_id, employ, "/employment").subscribe((data: any)=>{
        this.employmentForm.reset()
        this.router.navigateByUrl("/apply/bank")
     })

     /**
      * TODO
      * employ. = this.employmentForm .value.;
     employ. = this.employmentForm .value.;
     */
  }

  


}
