
export class PersonalParticulars {
    membership_no? : any;
    first_name?: string;
    middle_name?: string;
    last_name?: string;
    id_no_passport_no?: any;
    dob?: Date;
    home_address?: string;
    office_tel_no?: any;
    mobile_no?: any;
    pin_no?: any;
    email?: any;
    marital_status?: any;
    no_of_dependencies?: number; 
}

export class PhysicalAddress {
    town?: string;
    estate?: string;
    street_house_no?: any;
    no_of_years_lived?: number;
    no_of_months_lived?: number;
    rented_or_owned?: boolean;
    personal_particulars_id?: number
}

export class EmploymentDetails {
    applicant_employer?: string;
    postal_address?: string;
    physical_address?: string;
    designated?: string;
    tel_phone?: any;
    staff_number?: number;
    employment_terms?: any;
    personal_particulars_id?: number
}

export class BusinessDetails {
    business_income?: number;
    rental_income?: number;
    other_income?: number;
    personal_particulars_id?: number
}

export class BankDetails {
    account_name?: string;
    account_no?: any;
    bank_Code?:any;
    branch?: string;
    branch_code?: any;
    bank_Name?: string;
    personal_particulars_id?: number
}

export class LoanParticulars {
    loan_type?: string;
    purpose_of_loan?: string;
    amount_applied?: number;
    personal_particulars_id?: number

}



export class LoanOtherBank {
    name_of_institute?: string;
    amount_advanced?: number;
    personal_particulars_id?: number
    date_granted?: Date;
    repayment_period?: number;
    outstanding_balance?: number;
}