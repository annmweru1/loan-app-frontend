import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { LoanService } from '../services/loan.service';
import {LoanOtherBank} from '../models/loan.models'
import { Util } from '../common/util.common';
import { Constant } from '../common/const.common';

interface Loan {
  key: Number
  bankName: String
  amountAdvanced: Number
  dateGranted: String
  repaymentPeriod: String
  outstandingBalance: String
}


@Component({
  selector: 'app-otherbanks-form',
  templateUrl: './otherbanks-form.component.html',
  styleUrls: ['./otherbanks-form.component.css']
})
export class OtherbanksFormComponent implements OnInit {
  title = "Bank"
  util = new Util()
  constructor(
    private formBuilder: FormBuilder,
    private service: LoanService,
    private router: Router,
  ) {}

  otherBanks = this.formBuilder.group({
    bankName: "",
    amountAdvanced: "",
    dateGranted: "",
    repaymentPeriod: "",
    outstandingBalance: ""
  })

  loans: Loan[] = []
  keys: IterableIterator<number> = this.loans.keys()

  onSubmit() :void {
    console.log(this.otherBanks.value)

    if (this.loans.length == 0 ) {
      this.addToList()
    }

    let otherBank = new LoanOtherBank();
    otherBank.amount_advanced = this.otherBanks.value.amountAdvanced
    otherBank.name_of_institute = this.otherBanks.value.bankName
    otherBank.date_granted = this.otherBanks.value.dateGranted
    otherBank.repayment_period = this.otherBanks.value.repaymentPeriod
    otherBank.outstanding_balance = this.otherBanks.value.outstandingBalance


        
    let user_id = this.util.getCookie(Constant.ID_COOKIE_KEY)

    otherBank.personal_particulars_id = Number(user_id);

    this.service.update(user_id, otherBank, "/employment").subscribe((data: any)=>{
      this.otherBanks.reset()
      this.router.navigateByUrl("/apply/success")
    })


   
  }

  addToList(): void {
    let formValue = this.otherBanks.value;
    this.loans.push(formValue)
    this.keys = this.loans.keys()
    this.otherBanks.reset()
  }

  ngOnInit(): void {
  }

}

