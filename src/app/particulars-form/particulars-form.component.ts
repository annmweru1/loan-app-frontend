import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { Constant } from '../common/const.common';
import { Util } from '../common/util.common';
import { LoanParticulars } from '../models/loan.models';
import { LoanService } from '../services/loan.service';

@Component({
  selector: 'app-particulars-form',
  templateUrl: './particulars-form.component.html',
  styleUrls: ['./particulars-form.component.css']
})
export class ParticularsFormComponent implements OnInit {

  util = new Util()
  title = "Bank"
  constructor(
    private formBuilder: FormBuilder,
    private service: LoanService,
    private router: Router,
  ) {}
  particularsForm = this.formBuilder.group({
    loanType : "",
    purposeOfLoan : "",
    amountApplied: "",
  })

  onSubmit() :void {

    let particulars = new LoanParticulars()
    particulars.loan_type = this.particularsForm.value.loanType
    particulars.amount_applied = this.particularsForm.value.amountApplied
    particulars.purpose_of_loan = this.particularsForm.value.purposeOfLoan

    
        
    let user_id = this.util.getCookie(Constant.ID_COOKIE_KEY)

    particulars.personal_particulars_id = Number(user_id);

    this.service.update(user_id, particulars, "/loanpart").subscribe((data: any)=>{
      this.particularsForm.reset()
      this.router.navigateByUrl("/apply/otherbanks")
    })
  }


  ngOnInit(): void {
  }

}
