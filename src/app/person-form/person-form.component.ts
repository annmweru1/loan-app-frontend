import {Component} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { Constant } from "../common/const.common";
import { Util } from "../common/util.common";
import { PersonalParticulars } from "../models/loan.models";
import { LoanService } from "../services/loan.service";

@Component({
  selector: "app-personform",
  templateUrl: "./person-form.component.html",
  styleUrls: ["./person-form.component.css"],
})
export class PersonFormComponent {
  title = "Personal "
  util = new Util()
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: LoanService
  ) {}
    personForm = this.formBuilder.group({
      membershipNo: "",
      firstName: "",
      middleName: "",
      lastName : "",
      idPassport: "",
      dateOfBirth:"",
      homeAddress:"",
      officeTelNo:"",
      mobileNo:"",
      pinNo:"",
      email:"",
      maritalStatus :"",
      numberOfDependents: ""
    })

  onSubmit() :void {
   
   

    let person = new PersonalParticulars();
    person.dob = this.personForm.value.dateOfBirth
    person.email = this.personForm.value.email
    person.pin_no = this.personForm.value.pinNo
    person.first_name = this.personForm.value.firstName
    person.marital_status = this.personForm.value.maritalStatus
    person.middle_name = this.personForm.value.middleName
    person.office_tel_no = this.personForm.value.officeTelNo
    person.no_of_dependencies = this.personForm.value.numberOfDependents
    person.home_address = this.personForm.value.homeAddress
    person.membership_no = this.personForm.value.membershipNo
    person.id_no_passport_no = this.personForm.value.idPassport
    person.mobile_no = this.personForm.value.mobileNo
    person.last_name = this.personForm.value.lastName

  

    this.service.create(person).subscribe((data: any)=>{

      if(data.id != undefined) {
        this.util.setCookie(Constant.ID_COOKIE_KEY, data.id, 1)        
      } else {
        //TODO 
        //Show error to the user
      }
  

      this.router.navigateByUrl("/apply/physical")
    })

  }

}
