import {Component} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import { Constant } from "../common/const.common";
import { Util } from "../common/util.common";
import { PhysicalAddress } from "../models/loan.models";
import { LoanService } from "../services/loan.service";

@Component({
  selector: 'app-physical',
  templateUrl: './physical.component.html',
  styleUrls: ['./physical.component.css']
})
export class PhysicalComponent {
  title = "Physical "
  util = new Util()
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: LoanService
  ) {}
    personForm = this.formBuilder.group({
      town: "",
      estate: "",
      streetNo: "",
      yearsLived: "",
      monthsLived: "",
      rented: "",
    })

    onSubmit() :void {
     
      let addr = new PhysicalAddress()
      addr.estate = this.personForm.value.estate;
      /** Like you copy data from [personForm.value] to [addr]   wait  oka minute are we supposed to fill only the physicall fiels ama ? Yes weäre feeding the data to the service all the data?.yes the thing is we are using the same cursor so i cant work while you work too. iil work om my pc and paste hereok*/
      addr.no_of_months_lived = this.personForm.value.monthsLived
      addr.no_of_years_lived = this.personForm.value.yearsLived
      addr.town = this.personForm.value.town
      addr.rented_or_owned = this.personForm.value.yearsLived
      addr.street_house_no = this.personForm.value.streetNo

           
    let user_id = this.util.getCookie(Constant.ID_COOKIE_KEY)

    addr.personal_particulars_id = Number(user_id);

    this.service.update(user_id, addr, "/loanpart").subscribe((data: any)=>{
      this.personForm.reset()
      this.router.navigateByUrl("/apply/employment")
    })

    }
  
  }


