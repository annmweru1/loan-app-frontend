import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonalParticulars, PhysicalAddress, EmploymentDetails, BankDetails, BusinessDetails, LoanParticulars, LoanOtherBank } from '../models/loan.models'; 

const baseUrl = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<PersonalParticulars[]> {
    return this.http.get<PersonalParticulars[]>(baseUrl);
  }  
  get(id:any): Observable<PersonalParticulars> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    let url = baseUrl + "/personal"
    return this.http.post(url, data);
  }
  update(id:any,data:any, endpoint: string): Observable<any> {
    let url = baseUrl + endpoint
    return this.http.post(url, data);
  }
  delete(id: any): Observable<any> {
    return this.http.delete(`&{baseUrl}/${id}`);
  }
  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }
  findByTitle(title:any): Observable<PersonalParticulars[]> {
    return this.http.get<PersonalParticulars[]>(`${baseUrl}?title=${title}`);
  }
 
}
